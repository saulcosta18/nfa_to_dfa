This program allows for the input of non-deterministic finite automata through a CSV file for conversion to a DFA.

More comprehensive documentation to follow soon!

Released by Saul Costa (2014) under MIT License