# Graph structure for NFA to 

require 'colorize'
require 'graphviz'
require 'logger'

class Graph
  def initialize
    @states = {}
    @log = Logger.new(STDOUT)
  end

  def add_state(state_name)
    s_state_name = state_name.sort
    @log.debug "Wants to add"
    @log.debug s_state_name
    node_present = node_free(s_state_name)
    @log.debug "Nodefree says %s" % node_present
    if node_present
      @log.debug "Let it. There are now %d nodes." % @states.length
      @states[s_state_name] = {}
    end
  end

  # This will not allow duplicate values
  def add_transition(cur_transitions, final_state, transition)
    cur_transitions.map {|k,v| k}.each do |state|
      add_state(state)
    end
    present_transition = cur_transitions.find_all {|k,v| k == final_state }
    if present_transition.length > 0
      # @log.debug "Already some transitions"
      cur_transitions.select {|k,v| k == final_state }.each{|k,v| cur_transitions[k] = (v + ",%s" % transition) }
      ap cur_transitions
    else
      # @log.debug "NOT !!! already some transitions"
      ap cur_transitions.merge({final_state.sort => transition})
      cur_transitions.merge!({final_state.sort => transition})
    end
  end

  # Takes an array and returns the node with all its verticies or nil
  def get_node(s)
    node = @states.find_all {|k,v| (k & s).length == s.length and k.length == s.length}
    # @log.debug "found this for ya"
    # @log.debug node
    node.empty? ? nil : node[0]
  end

  def node_free(s)
    node = @states.find_all {|k,v| k === s}
     node.empty?
  end

  def display
    graph_hash = {}
    g = GraphViz.new("G", "type" => "digraph")
    # Add all the nodes
    @states.keys.each do |state|
      node = g.add_nodes(state.join(", "))
      graph_hash.merge!(state.join(", ") => node)
    end
    # Create the edges
    cur = 0
    total = @states.keys.length
    @states.keys.each do |state|
      get_node(state)[1].map{ |k,v| {:state => k, :symbol => v} }.each do |transition|
        @log.debug "Adding an edge between %s and %s labeled with %s" % [state.join(", "), transition[:state].join(", "), transition[:symbol]]
        edge = g.add_edges(graph_hash[state.join(", ")], graph_hash[transition[:state].join(", ")])
        edge[:label] = transition[:symbol]
      end
      cur += 1
      g.output(png: "DFA.png")
      @log.info "%d/%d done with graph rendering.".red % [cur, total]
    end
  end

  def states
    @states
  end
end
