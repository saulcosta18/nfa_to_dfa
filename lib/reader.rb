=begin
Read a CSV as transitions for delta.

Format of file:
  init_state, final_state, transition

Example:
q0, q1, 0 equates to (q0)-0->(q1)

Returns (eg):
{
  :sigma => {0,1},
  :init => ["q0"],
  :final => ["q0","q1"],
  :delta => [
    {"q0","q1",0},
    {"q0","q1","l"}
  ]
}

Special cases:
=> First three lines are reserved for sigma, NFA initial state and {NFA final_state(s)}
    s,0,1
    i,q0
    f,q1,q0
=> 0 in (init_state|final_state) denotes the empty set (returns "0")
=> l in tranition denotes lambda (returns -1)
=end

require 'csv'

class Reader
  def initialize
    @sigma = nil
    @init = nil
    @final = nil
    @delta = []
  end

  def read_file(filepath)
    CSV.foreach(filepath) do |row|
      case row[0]
      when "s"
        @sigma = row.drop(1)
      when "i"
        @init = row.drop(1)[0]
      when "f"
        @final = row.drop(1)
      else
        @delta.push({:init_state =>row[0], :final_state => row[1], :transition => row[2]})
      end
    end

    {
      :sigma => @sigma,
      :init => @init,
      :final => @final,
      :delta => @delta.uniq
    }
  end
end