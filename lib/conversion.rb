#!/usr/local/rvm/rubies/ruby-1.9.3-p392/bin/ruby

=begin
Convert hash of NFA states and transitions into DFA

Input: hashmap of formal description (See reader.rb)
Output: file with formal description of DFA, including delta
=end

require 'awesome_print'
require 'logger'
require_relative 'graph'
require_relative 'reader'

class Conversion

  def initialize(filepath)
    r = Reader.new
    formal_desc = r.read_file(filepath)
    @sigma = formal_desc[:sigma]
    @init = formal_desc[:init]
    @final = formal_desc[:final]
    @delta = formal_desc[:delta].sort_by { |t| t[:init_state] }
    # @q = TODO: Is this needed?
    @dfa = Graph.new
    @log = Logger.new(STDOUT)
    @finished = ["null"]
    @found = false
    # @num_states = 1
  end

  # Find delta*(init_state, transition)
  # Returns an array
  def check_transition(start_state, transition)
    data = @delta
    data.group_by {|i| i[:init_state]}[start_state].find_all{|t| t[:transition] == transition.to_s}.map{|k| k[:final_state]}
  end

  def convert
    # Check if there are any lamba transitions from @init to other states
    l_states = check_transition(@init, 'l')
    init_states = [@init] | l_states
    # Add initial state(s)
    @log.debug "sigma length is %d" % @sigma.length
    @dfa.add_state(init_states)
    add_transition(init_states)
    start
    add_transition(["null"])
    ap @dfa.states
    @dfa.display
    ap finished
  end

  def start
    add_transition(get_next_state[0])
    if ensure_processed
      return
    else
      # @log.debug "NOT DONE: there are %s in the last state" % get_next_state[1].length
      start
    end
  end

  def ensure_processed
    @log.debug "EP"
    @log.debug get_next_state
    get_next_state[1].length == @sigma.length ? true : false
  end

  def num_states
   @dfa.states.length
  end

  def finished
    @finished
  end

  # Return the next state that requires processing
  def get_next_state
    fin = finished
    c = @sigma.length
    least_done = @dfa.states.find_all {|k,v| !(fin.include?(k.join(""))) }.first
    if least_done.nil?
      return ["done", @sigma]
    end
    if least_done[1].map {|k,v| v}.join("").gsub(",","").length < c
      @log.debug "LOOKIE %s" % least_done[1].map {|k,v| v}.join("").gsub(",","")
      return least_done
    else
      @log.debug "LOOKIE" 
      @log.debug least_done
      return ["done", @sigma]
    end
  end

  # Will check that state has |@sigma| transitions leaving from it
  # Will recursively check that all of the remaining states do as well
  def add_transition(state)
    if state.empty?
      @log.debug "returning because state is empty"
      return
    end
    c = @sigma.length
    @dfa.add_state(state)
    node = @dfa.get_node(state)
    @log.debug node
    # if node.nil?
    #   @log.debug "returned becau"
    #   return
    # end
    if node[1].map {|k,v| v}.join("").gsub(",","").length >= c
      @log.debug "All transitions present: %s" % node[1].map {|k,v| v}.join("")
      return
    end
    if node[0] == ["null"]
      @sigma.each do |s|
        @dfa.add_transition(node[1], ["null"], s)
      end
      return
    else
      (@sigma - node[1].to_a).to_a.each do |symbol|
        # @log.debug "checking %s" % symbol
        final_states = []
        # sts = (node[0].join(",").split(","))
        node[0].each do |init_state|
          # @log.debug "init state is"
          # @log.debug init_state
          dests = @delta.find_all {|t| t[:init_state] == init_state.to_s and t[:transition] == symbol.to_s}.map{|k| k[:final_state]}

          final_states = final_states | dests
        end
        # Now check for lamba
        final_states.each do |state|
          lambda_states = @delta.find_all {|t| t[:init_state] == state.to_s and t[:transition] == "l"}.map{|k| k[:final_state]}
          unless lambda_states.empty?
            final_states = final_states | lambda_states
          end
        end
        if final_states.empty?
          @dfa.add_state(["null"])
          @dfa.add_transition(node[1], ["null"], symbol)
        else
          @dfa.add_state(final_states)
          ap final_states.sort
          @dfa.add_transition(node[1], final_states.sort, symbol)
        end
      end # each symbol
    end
    @finished.push(node[0].join(""))
    # if node[0].join("") == "058"
    #   @found = true
    # elsif @dfa.states.select {|k,v| k == ["0", "5", "8"]}.values[0].empty? and @found == true
    #   abort
    # end
  end

  def convert_smart
  end
end