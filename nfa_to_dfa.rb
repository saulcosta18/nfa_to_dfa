# Provide command line usage for NFA to DFA conversion
# Purpose: Test speed of using previously discovered transitions
#   as opposed to recomputing them for each possible state.

require 'optparse'
require 'require_all'
require_all './lib'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: nfa_to_dfa.rb [options]"

  opts.on("-f", "CSV file describing NFA") do |f|
    options[:file] = f
  end
  opts.on("-c", "Change computation mode") do |c|
    options[:mode] = c
  end
end.parse!
@program = Conversion.new(ARGV[0])
@program.convert
